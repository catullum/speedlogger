package com.fabi.speedlogger;

import android.provider.BaseColumns;

public final class deviceReader {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private deviceReader() {}

    /* Inner class that defines the table contents */

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "device";
        public static final String COLUMN_NAME_DEVICE_NAME = "name";
        public static final String COLUMN_NAME_MAC_ADRESS = "adress";
    }





}
